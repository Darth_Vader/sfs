from disk_library import openDisk
from disk_library import writeBlock
from disk_library import readBlock

disk = openDisk("disk.txt",0)

writeBlock(disk,0,bytearray('AB','utf8'))

x = readBlock(disk,0)

print(x)
